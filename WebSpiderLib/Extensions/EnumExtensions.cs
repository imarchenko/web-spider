﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebSpiderLib.Attributes;
using WebSpiderLib.Models;

namespace WebSpiderLib.Extensions
{
    public static class EnumExtensions
    {
        public static string GetLocalizedDisplayString(this Enum e)
        {
            if (e == null)
            {
                throw new ArgumentNullException("e");
            }
            var type = e.GetType();
            var enumInfo = type.GetMember(Enum.GetName(type, e)).FirstOrDefault();
            if (enumInfo == null)
            {
                return string.Empty;
            }

            var displayAttribute =
                enumInfo.GetCustomAttributes(typeof(DisplayAttribute), false)
                .OfType<DisplayAttribute>()
                .FirstOrDefault();

            return displayAttribute == null ? string.Empty : displayAttribute.GetName();
        }

        public static TReturnValue GetCustomAttribute<TReturnValue, TAttribute>(Enum e, TReturnValue defaultValue)
            where TAttribute : class, ICustomAttribute<TReturnValue>
        {
            if (e == null)
            {
                throw new ArgumentNullException("e");
            }
            var type = e.GetType();
            var enumInfo = type.GetMember(Enum.GetName(type, e)).FirstOrDefault();
            if (enumInfo == null)
            {
                return defaultValue;
            }

            var customAttribute =
                enumInfo.GetCustomAttributes(typeof(TAttribute), false)
                .OfType<TAttribute>()
                .FirstOrDefault();

            return customAttribute == null ? defaultValue : customAttribute.Get();
        }
        
        public static CellDataType GetCellDataType(this Enum e)
        {
            const CellDataType defaultValue = CellDataType.Text;
            var value = GetCustomAttribute<CellDataType, ParseLikeAttribute>(e, defaultValue);
            return value;
        }

        public static string GetIconName(this Enum e)
        {
            const string defaultValue = "unknown_scan.png";
            var value = GetCustomAttribute<string, ScanIconAttribute>(e, defaultValue);
            return value;
        }

        public static string GetDescription(this Enum e)
        {
            var defaultValue = GetLocalizedDisplayString(e);
            var value = GetCustomAttribute<string, ScanDescriptionAttribute>(e, defaultValue);
            return value;
        }

        public static List<T> ToList<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToList();
        }
    }
}