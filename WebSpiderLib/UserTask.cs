﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;
using System;
using WebSpiderLib.Models;
using WebSpiderLogger;
using System.Net;
using WebSpiderLib.Extensions;
using WebSpiderLogger.Models;

namespace WebSpiderLib
{
    public class UserTask : IDisposable
    {
        private string SseStreamUrl { get; set; }

        private string CheckedHost { get; set; }

        private SeoParameter Parameter { get; set; }

        private int RowId { get; set; }

        private const string DefaultScheme = "http://";

        private const string SecurityScheme = "https://";

        private Logger _logger;

        private const int RequestTimeoutInMs = 300000; // 5 мин.

        private ExtendedWebClient _webClient;

        public UserTask(int rowId, string sseStreamUrl, string checkedHost, SeoParameter parameter, Logger logger)
        {
            RowId = rowId;
            SseStreamUrl = sseStreamUrl;
            CheckedHost = checkedHost;
            Parameter = parameter;
            _logger = logger;
            _webClient = new ExtendedWebClient(RequestTimeoutInMs);
        }

        private string FirstLetterToLower(string str)
        {
            if (String.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            var charArray = str.ToCharArray();
            charArray[0] = Char.ToLower(charArray.First());
            var result = new string(charArray);
            return result; 
        }

        public SSEvent Start()
        {
            if (String.IsNullOrEmpty(SseStreamUrl) || String.IsNullOrEmpty(CheckedHost))
            {
                return null;
            }
            if (SseStreamUrl.EndsWith("/"))
            {
                SseStreamUrl = SseStreamUrl.Remove(SseStreamUrl.Length - 1);
            }
            var urlScheme = "http";
            if (CheckedHost.StartsWith(SecurityScheme))
            {
                urlScheme = "https";
                CheckedHost = CheckedHost.Replace(SecurityScheme, string.Empty);
            }
            else
            {
                CheckedHost = CheckedHost.Replace(DefaultScheme, string.Empty);
            }
            var parameter = FirstLetterToLower(Parameter.ToString());
            var url = string.Format("{0}/{1}/{2}/{3}/111", SseStreamUrl, parameter, urlScheme, CheckedHost);
            var response = AppSettings.Default.TaskResultError;
            try
            {
                response = _webClient.DownloadString(url);
            }
            catch (WebException ex)
            {
                var msgTitle = String.Format("Проверка параметра {0} сайта {1}: ", Parameter.GetLocalizedDisplayString(), CheckedHost);
                if (ex.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    _logger.AddFatalError(FatalErrorType.ApiConnectionFailed, FatalErrorType.ApiConnectionFailed.GetLocalizedDisplayString());
                }
                else if (ex.Status == WebExceptionStatus.Timeout)
                {
                    _logger.AddError(msgTitle + "Время ожидания ответа от сервера превысило " + RequestTimeoutInMs/1000/60 + " мин.");
                }
                else
                {
                    _logger.AddError(msgTitle + ex.Message);
                }
            }
            catch (Exception ex)
            {
                var msg = string.Format("Ошибка при попытке получить данные по параметру '{0}' на сайте '{1}':",
                    Parameter.GetLocalizedDisplayString(), CheckedHost);
                _logger.AddError(msg + ex.Message);
            }
            var ssEvent = new SSEvent
            {
                Parameter = Parameter,
                Data = response
            };
            return ssEvent;
        }

        public ApiResult ParseJson(SSEvent ssEvent)
        {
            var dataType = ssEvent.Parameter.GetCellDataType();
            var result = new ApiResult(RowId, CheckedHost, dataType)
            {
                TaskResult = new ApiTask(ssEvent.Parameter) { Result = ssEvent.Data }
            };
            if (ssEvent.Data == AppSettings.Default.TaskResultError)
            {
                return result;
            }
            try
            {
                var apiTask = JsonConvert.DeserializeObject<ApiTask>(ssEvent.Data);
                apiTask.Result = DeserializeTaskResult(apiTask.Id.GetCellDataType(), apiTask.Result.ToString());
                result.TaskResult = apiTask;
            }
            catch (JsonReaderException)
            {
                _logger.AddError(String.Format("Параметр {0} сайта {1}: Ошибка формата данных полученных от сервера",
                    Parameter.GetLocalizedDisplayString(), CheckedHost));
            }
            return result;
        }

        public void Dispose()
        {
            _webClient.Dispose();
        }

        private string DeserializeTaskResult(CellDataType dataType, string data)
        {
            switch (dataType)
            {
                case CellDataType.Text:
                    return data;
                case CellDataType.Boolean:
                    return DeserializeAsBoolean(data) ?? data;
                case CellDataType.Number:
                    return DeserializeAsNumber(data) ?? data;
                case CellDataType.Date:
                case CellDataType.JsonDictionary:
                    return DeserializeAsBoolean(data) ?? DeserializeAsLines(data) ?? data;
                default:
                    return data;
            }
        }

        private string DeserializeAsLines(string json)
        {
            try
            {
                var dictionary = new Dictionary<string, string>();
                var result = String.Join("\n", JsonConvert.DeserializeAnonymousType(json, dictionary)
                    .Select(item => String.Format("{0}: {1}", item.Key, item.Value)));
                return result;
            }
            catch (JsonReaderException)
            {
                _logger.AddError(String.Format("Параметр {0} сайта {1}: Не удается распарсить json '{2}'",
                           Parameter.GetLocalizedDisplayString(), CheckedHost, json));
                return null;
            }
        }

        private string DeserializeAsBoolean(string data)
        {
            bool flag;
            if (Boolean.TryParse(data, out flag))
            {
                return flag ? "Есть" : "Нету";
            }
            _logger.AddError(String.Format("Параметр {0} сайта {1}: Не удается преобразовать в логическое значение '{2}'",
                Parameter.GetLocalizedDisplayString(), CheckedHost, data));
            return null;
        }

        private string DeserializeAsNumber(string data)
        {
            int number;
            if (Int32.TryParse(data, out number))
            {
                return number.ToString(CultureInfo.InvariantCulture);
            }
            _logger.AddError(String.Format("Параметр {0} сайта {1}: Не удается преобразовать в число '{2}'",
                Parameter.GetLocalizedDisplayString(), CheckedHost, data));
            return null;
        }
    }
}
