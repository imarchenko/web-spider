﻿using System.ComponentModel;
using System.Security.Cryptography;

namespace WebSpiderLib.Attributes
{
    public class ScanDescriptionAttribute : DescriptionAttribute, ICustomAttribute<string>
    {
        public ScanDescriptionAttribute() : base() { }

        public ScanDescriptionAttribute(string description) : base(description) { }

        public string Get()
        {
            return DescriptionValue;
        }
    }
}
