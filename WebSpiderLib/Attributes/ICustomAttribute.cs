﻿namespace WebSpiderLib.Attributes
{
    public interface ICustomAttribute<out T>
    {
        T Get();
    }
}
