﻿using System;
using System.CodeDom;

namespace WebSpiderLib.Attributes
{
    public class ScanIconAttribute : Attribute, ICustomAttribute<string>
    {
        private readonly string _name;

        /// <param name="iconName">имя файла иконки (например: icon.png)</param>
        public ScanIconAttribute(string iconName)
        {
            _name = iconName;
        }

        public string Get()
        {
            return _name;
        }
    }
}
