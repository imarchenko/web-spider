﻿using System;
using WebSpiderLib.Models;

namespace WebSpiderLib.Attributes
{
    public class ParseLikeAttribute : Attribute, ICustomAttribute<CellDataType>
    {
        private readonly CellDataType _dataType;

        public ParseLikeAttribute(CellDataType dataType)
        {
            _dataType = dataType;
        }

        public CellDataType Get()
        {
            return _dataType;
        }
    }
}
