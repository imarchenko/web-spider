﻿using System.ComponentModel.DataAnnotations;
using WebSpiderLib.Attributes;

namespace WebSpiderLib.Models
{
    public enum SeoParameter
    {
        // ReSharper disable InconsistentNaming
        [Display(Name = "Adblock контент")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Adblock_content.png")]
        [ScanDescription("Это Adblock контент")]
        Adblock_content,
        [Display(Name = "Addthis")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Addthis.png")]
        [ScanDescription("Это Addthis")]
        Addthis,
        [Display(Name = "Админ панель")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Admin_panel.png")]
        Admin_panel,
        [Display(Name = "Alexa Rank")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Alexa_rank.png")]
        Alexa_Rank,
        [Display(Name = "Роскомнадзор")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Antizapret.png")]
        Antizapret,
        [Display(Name = "Архив")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Archive.png")]
        Archive,
        [Display(Name = "Bing индекс")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Bing_index.png")]
        Bing_index,
        [Display(Name = "YaCa")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("YaCa.png")]
        Yaca,
        [Display(Name = "Dmoz")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Dmoz.png")]
        Dmoz,
        [Display(Name = "Блог")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Blog.png")]
        Blog,
        [Display(Name = "Кодировка сайта")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Encoding.png")]
        Charset,
        [Display(Name = "Online chats")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Online_chats.png")]
        Chats,
        [Display(Name = "DNS")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("DNS.png")]
        Dns,
        [Display(Name = "Возраст домена")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Domain_age.png")]
        Domain_age,
        [Display(Name = "Мгновенный поиск домена")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Instant_domain_search.png")]
        Domain_availability,
        [Display(Name = "Доменная зона")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Domain_zone.png")]
        Domen_zone,
        [Display(Name = "DrWeb")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("DrWeb.png")]
        Drweb,
        [Display(Name = "Email grabber")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Email_grabber.png")]
        Email_grabber,
        [Display(Name = "Внешние ссылки")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Vneshnie_ssilki.png")]
        External_links,
        [Display(Name = "Наличие Favicon")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Favicon.png")]
        Favicon,
        [Display(Name = "Поиск Репозиториев")]
        [ParseLike(CellDataType.Text)]
        Find_repos,
        [Display(Name = "Форум")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Forum.png")]
        Forum,
        [Display(Name = "Репозитории github.com")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Repozitorii_github.png")]
        Github_repository,
        [Display(Name = "Склейка домена")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Gluing_together.png")]
        Gluing_together,
        [Display(Name = "Бан в google adsense")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Ban_in_Google_Adsense.png")]
        Google_adsense_ban,
        [Display(Name = "Google авторство")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Google_author.png")]
        Google_author,
        [Display(Name = "Конкуренты по мнению google")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Competition_for_Google_oppinion.png")]
        Google_competitors,
        [Display(Name = "Упоминания в facebook.com")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("References_in_facebook.png")]
        Google_facebook,
        [Display(Name = "Google изображения")]
        [ParseLike(CellDataType.Number)]
        [ScanIcon("Google_images.png")]
        Google_images,
        [Display(Name = "Google индекс")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Google_index.png")]
        Google_index,
        [Display(Name = "Основной индекс google")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Google_main_index.png")]
        Google_main_index,
        [Display(Name = "Упоминания в ok.ru")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("References_ok.png")]
        Google_ok,
        [Display(Name = "Google PR")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("GooglePR.png")]
        Google_pr,
        [Display(Name = "Google Safebrowsing")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Google_safebrowsing.png")]
        Google_safebrowsing,
        [Display(Name = "Google PageSpeed")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Google_page_speed.png")]
        Google_speedTest,
        [Display(Name = "Структурированные данныe")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Structured_data.png")]
        Google_testing_tool,
        [Display(Name = "Упоминания в twitter.com")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Upominanie_v_twetter.png")]
        Google_twitter,
        [Display(Name = "Упоминания в vk.com")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Upominanie_v_vk.png")]
        Google_vk,
        [Display(Name = "Google вебмастер")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Google_webmaster.png")]
        Google_webmaster,
        [Display(Name = "Headers")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Headers.png")]
        Headers,
        [Display(Name = "Заголовки")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Titles.png")]
        Headings,
        [Display(Name = "htaccess")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("htaccess.png")]
        Htaccess,
        [Display(Name = "Humans_txt")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Humans_txt.png")]
        Humans_txt,
        [Display(Name = "Iframes")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("iframes.png")]
        Iframes,
        [Display(Name = "Изображения")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Images.png")]
        Images,
        [Display(Name = "ip canonization")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Ip_canonization.png")]
        Ip_canonization,
        [Display(Name = "IP адрес")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("IP.png")]
        Ip_info,
        [Display(Name = "Соседи по IP")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Neghbors_by_IP.png")]
        Ip_neighbors,
        [Display(Name = "Like Explorer")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Like_explorer.png")]
        Like_explorer,
        [Display(Name = "Ссылки")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Links.png")]
        Links,
        [Display(Name = "Liveinternet")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Liveinternet.png")]
        Liveinternet,
        [Display(Name = "Поиск логов")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Search_logs.png")]
        Logs,
        [Display(Name = "Каталог mail.ru")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("mail.ru_catalog.png")]
        Mail_ru_catalog,
        [Display(Name = "Majestic графики")]
        [ParseLike(CellDataType.Text)]
        Majestic_graphics,
        [Display(Name = "Mobile friendly")]
        [ParseLike(CellDataType.Text)]
        Mobile_friendly,
        [Display(Name = "Mozrank")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Mozrank.png")]
        Mozrank,
        [Display(Name = "Nikto Denial of Service")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Nikto_denial_of_service.png")]
        Nikto_denial_service,
        [Display(Name = "Nikto File Upload")]
        [ParseLike(CellDataType.Text)]
        Nikto_file_upload,
        [Display(Name = "Nikto Information Disclosure")]
        [ParseLike(CellDataType.Text)]
        Nikto_information_disclosure,
        [Display(Name = "Nikto XSS/Script/HTML")]
        [ParseLike(CellDataType.Text)]
        Nikto_injection_xss_html,
        [Display(Name = "Interesting File")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Interesting_file.png")]
        Nikto_interesting_file,
        [Display(Name = "Nikto Misconfiguration")]
        [ParseLike(CellDataType.Text)]
        Nikto_misconfiguration,
        [Display(Name = "Nikto Remote File")]
        [ParseLike(CellDataType.Text)]
        Nikto_remote_file,
        [Display(Name = "Nikto File Retrieval - Server Wide")]
        [ParseLike(CellDataType.Text)]
        Nikto_retrieval_server,
        [Display(Name = "Nikto Remote Shell")]
        [ParseLike(CellDataType.Text)]
        Nikto_shell,
        [Display(Name = "Nikto SQL Injection")]
        [ParseLike(CellDataType.Text)]
        Nikto_sql_injection,
        [Display(Name = "Проверка на бэкапы")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Proverka_na_backup.png")]
        Nmap_http_config_backup,
        [Display(Name = "http default account")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("http_default_accounts.png")]
        Nmap_http_default_accounts,
        [Display(Name = "Запрос к серверу microsoft sql")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Zapros_k_serveru_microsoft_sql.png")]
        Nmap_ms_sql,
        [Display(Name = "Yandex noindex")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Yandex_noindex.png")]
        Noindex,
        [Display(Name = "Поиск php.info")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Searching_php.info.png")]
        Php_info,
        [Display(Name = "Сканер портов")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Ports_scan.png")]
        Pscan,
        [Display(Name = "robots.txt")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Robots.png")]
        Robots_txt,
        [Display(Name = "RSS")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("RSS.png")]
        Rss,
        [Display(Name = "Основные цвета")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Main_colors.png")]
        Site_colors,
        [Display(Name = "Карта сайта")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Site_map.png")]
        Site_map,
        [Display(Name = "Вес сайта")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Ves_site.png")]
        Site_weight,
        [Display(Name = "Site Advisor")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Site_advisor.png")]
        Siteadvisor,
        [Display(Name = "Skype")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Skype.png")]
        Skype,
        [Display(Name = "Поиск sql базы")]
        [ParseLike(CellDataType.JsonDictionary)]
        [ScanIcon("Sql_database_search.png")]
        Sql_database,
        [Display(Name = "Ssl сертификат")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Ssl_sertefikat.png")]
        Ssl,
        [Display(Name = "Под домены")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Subdomains.png")]
        Sub_domains,
        [Display(Name = "Sucuri Black List")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Sucuri_Black_List.png")]
        Sucuri_blacklist,
        [Display(Name = "Рейтинг@mail.ru")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Top_mail_ru.png")]
        Top_mail_ru,
        [Display(Name = "Топ openstat.ru")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Top_openstat.ru.png")]
        Top_openstat_ru,
        [Display(Name = "Топ 100 rambler.ru")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Top100 rambler.ru.png")]
        Top_rambler_ru,
        [Display(Name = "Разметка")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Markup.png")]
        Validator_w3,
        [Display(Name = "СSS валидация")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("СSS_validation.png")]
        Validator_w3_css,
        [Display(Name = "Virus total")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("VirusTotal.png")]
        Virus_total,
        [Display(Name = "Whois")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Whois.png")]
        Whois,
        [Display(Name = "WM Arbitage")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("WM_Arbitage.png")]
        Wm_arbitage,
        [Display(Name = "Популярные слова")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Popular_words.png")]
        Words,
        [Display(Name = "Репутация WOT")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("WOT.png")]
        Wot,
        [Display(Name = "Yahoo индекс")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Yahoo_index.png")]
        Yahoo_index,
        [Display(Name = "Блоги яндекс")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Yandex_blogs.png")]
        Yandex_blogs,
        [Display(Name = "Яндекс изображения")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Yandex_images.png")]
        Yandex_images,
        [Display(Name = "Яндекс вирус")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Yandex_virus.png")]
        Yandex_infected,
        [Display(Name = "Яндекс ТИЦ")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Yandex_TIC.png")]
        Yandex_tiq,
        [Display(Name = "Яндекс вебмастер")]
        [ParseLike(CellDataType.Boolean)]
        [ScanIcon("Yandex_webmaster.png")]
        Yandex_webmaster,
        [Display(Name = "Youtube поиск")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Youtube_search.png")]
        Youtube_search,
        [Display(Name = "ZeuS Tracker")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Zeus_tracker.png")]
        Zeus,
        [Display(Name = "Сервисы на сервере")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Servis_on_server.png")]
        Server_services,
        [Display(Name = "Входящие сслыки")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Vhodyashie_ssilki.png")]
        Incoming_links,
        [Display(Name = "Проверка на наличие вредоноса")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Proverka_na_nalichie_vredonosa.png")]
        Malware_host,
        [Display(Name = "ftp anon")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Ftp_anon.png")]
        Nmap_ftp_anon,
        [Display(Name = "Поиск запароленных моментов")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Poisk_zaparolennih_momentov.png")]
        Nmap_http_method_tamper,
        [Display(Name = "Поиск шелов")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Poisk_shellov.png")]
        Shell_finder,
        [Display(Name = "Поиск файла инсталяции")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Find_file_installation.png")]
        Install_path,
        [Display(Name = "Рекламные сети")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Reklamnie_seti.png")]
        W_adv_net,
        [Display(Name = "Аналитика")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Analitics.png")]
        W_analytics,
        [Display(Name = "Блоги")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Blog.png")]
        W_blogs,
        [Display(Name = "Кэширование")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Cash.png")]
        W_cache_tools,
        [Display(Name = "Каптчи")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Captcha.png")]
        W_captchas,
        [Display(Name = "CDN")]
        [ParseLike(CellDataType.Text)]
        W_cdn,
        [Display(Name = "CMS")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("CMS.png")]
        W_cms,
        [Display(Name = "Комментарии")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Comments.png")]
        W_comment_syst,
        [Display(Name = "Базы данных")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Database.png")]
        W_database,
        [Display(Name = "Менеджер базы данных")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Database_manager.png")]
        W_database_manager,
        [Display(Name = "Средства документации")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Documentation_tools.png")]
        W_doc_tools,
        [Display(Name = "Электронная коммерция")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("ECommerce.png")]
        W_ecommerce,
        [Display(Name = "Редакторы")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Text_editors.png")]
        W_editors,
        [Display(Name = "Скрипты шрифтов")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Font_scripts.png")]
        W_font_scripts,
        [Display(Name = "Хостинговая панель")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Hosting_panel.png")]
        W_hosting_panel,
        [Display(Name = "Управления проэктами")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Managing_projects.png")]
        W_issue_trackers,
        [Display(Name = "JS фреймворки")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("JS_frameworks.png")]
        W_js_frameworks,
        [Display(Name = "JS графика")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("JS_graphics.png")]
        W_js_graphic,
        [Display(Name = "LMS")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("LMS.png")]
        W_lms,
        [Display(Name = "Карты")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Maps.png")]
        W_maps,
        [Display(Name = "Автоматизация маркетинга")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Marketing_automatization.png")]
        W_marketing_avtomation,
        [Display(Name = "Доска сообщений")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Message_boards.png")]
        W_message_boards,
        [Display(Name = "Медиа сервер")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Media_servers.png")]
        W_media_servers,
        [Display(Name = "Мобильные фрэймворки")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Mobile_frameworks.png")]
        W_mobile_frameworks,
        [Display(Name = "ОС")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("OS.png")]
        W_operation_sys,
        [Display(Name = "Платежные системы")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Pay_systems.png")]
        W_pay_proc,
        [Display(Name = "Фото галереи")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Photo_gallery.png")]
        W_photo_gallery,
        [Display(Name = "Принтеры")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Printers.png")]
        W_printers,
        [Display(Name = "Язык программирования")]
        [ParseLike(CellDataType.Text)]
        [ScanDescription("Язык программирования")]
        [ScanIcon("Programming_language.png")]
        W_proramming_lang,
        [Display(Name = "Текстовые редакторы")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Text_redaktor.png")]
        W_rich_editors,
        [Display(Name = "Поисковая инженерия")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Searching_engeneer.png")]
        W_search_engine,
        [Display(Name = "Видеопроигрыватели")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Videoplayers.png")]
        W_video_players,
        [Display(Name = "Widgets")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Widgets.png")]
        W_widgets,
        [Display(Name = "Web frameworks")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Web_frameworks.png")]
        W_web_fr,
        [Display(Name = "Веб-почта")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Web_mail.png")]
        W_web_mail,
        [Display(Name = "Расширения веб сервера")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Rashirenie_webserverf.png")]
        W_web_serv_ext,
        [Display(Name = "Вэб сервер")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Web_server.png")]
        W_web_servers,
        [Display(Name = "Web камеры")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Web_cam.png")]
        W_webcams,
        [Display(Name = "Wikis")]
        [ParseLike(CellDataType.Text)]
        [ScanIcon("Wikis.png")]
        W_wikis
    }
}
