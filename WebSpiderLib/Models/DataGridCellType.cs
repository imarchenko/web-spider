﻿using System.ComponentModel.DataAnnotations;

namespace WebSpiderLib.Models
{
    public enum DataGridCellType
    {
        [Display(Name = "#")]
        Id,
        [Display(Name = "Проверка/Сайт")]
        Scan_SiteName
    }
}
