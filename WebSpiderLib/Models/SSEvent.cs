﻿namespace WebSpiderLib.Models
{
    public class SSEvent
    {
        public SeoParameter Parameter { get; set; }

        public string Data { get; set; }
    }
}
