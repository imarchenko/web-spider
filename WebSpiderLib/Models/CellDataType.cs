﻿namespace WebSpiderLib.Models
{
    public enum CellDataType
    {
        Text,
        Boolean,
        Number,
        Date,
        JsonDictionary
    }
}
