﻿namespace WebSpiderLib.Models
{
    public class ApiResult
    {
        public int RowId { get; private set; }

        public string Host { get; private set; }

        public ApiTask TaskResult { get; set; }

        public CellDataType DataType { get; private set; }

        public ApiResult()
        {
            DataType = CellDataType.Text;
        }

        public ApiResult(int rowId, string host, CellDataType dataType) : this()
        {
            RowId = rowId;
            Host = host;
            DataType = dataType;
        }
    }
}
