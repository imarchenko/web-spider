﻿namespace WebSpiderLib.Models
{
    public class ApiTask
    {
        public SeoParameter Id { get; set; }

        public double Duration { get; set; }

        public string Host { get; set; }

        public object Result { get; set; }

        public ApiTask()
        {

        }

        public ApiTask(SeoParameter id) : this()
        {
            Id = id;
        }
    }
}
