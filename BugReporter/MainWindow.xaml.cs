﻿using System.Diagnostics;
using System.IO;
using System.Windows;
using BugReporter.Services;

namespace BugReporter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string MainApplcationFileName = "Web Spider WPF.exe";
        const string MainApplcationLogName = "Logs.txt";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Exit_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void RestartMainApp_OnClick(object sender, RoutedEventArgs e)
        {
            if (IsSendReportEnabled.IsChecked.GetValueOrDefault() && File.Exists(MainApplcationLogName))
            {
                ReportSendingProgressBar.Visibility = Visibility.Visible;
                RestartMainAppButton.IsEnabled = false;
                var logs = File.ReadAllText(MainApplcationLogName);
                var emailService = new EmailService();
                emailService.OnEmailSent += emailService_OnEmailSent;
                emailService.SendReport(logs, UserEmail.Text, UserComment.Text);
            }
            else
            {
                RestartMainApp();
            }
        }

        void emailService_OnEmailSent()
        {
            RestartMainApp();
        }

        private void RestartMainApp()
        {
            if (File.Exists(MainApplcationFileName))
            {
                Process.Start(MainApplcationFileName);
                Close();
            }
        }

        private void IsSendReportEnabled_OnClick(object sender, RoutedEventArgs e)
        {
            ChangeSendReportStatus();
        }

        private void ChangeSendReportStatus()
        {
            if (IsSendReportEnabled.IsChecked.GetValueOrDefault())
            {
                UserEmail.IsEnabled = true;
                UserComment.IsEnabled = true;
            }
            else
            {
                UserEmail.IsEnabled = false;
                UserComment.IsEnabled = false;
            }
        }
    }
}
