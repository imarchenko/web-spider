﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Windows;

namespace BugReporter.Services
{
    public class EmailService
    {
        public event Action OnEmailSent; 

        public void SendReport(string log, string userEmail, string userComment)
        {
            var body = string.Format("Комментарий от {0}:\n{1}\n\nЛоги:\n{2}", userEmail, userComment, log);
            var message = new MailMessage(AppSettings.Default.SmtpLogin, AppSettings.Default.SendTo, AppSettings.Default.Subject, body);
            SendEmailAsync(message);
        }


        private void SendEmailAsync(MailMessage mailMessage)
        {
            using (var smtpClient = new SmtpClient(AppSettings.Default.SmtpHost)
            {
                Port = AppSettings.Default.SmtpPort,
                EnableSsl = true,
                Credentials = new NetworkCredential(AppSettings.Default.SmtpLogin, AppSettings.Default.SmtpPassword)
            })
            {
                try
                {
                    //smtpClient.SendCompleted += smtpClient_SendCompleted;
                    //smtpClient.SendAsync(mailMessage, null);
                    smtpClient.Send(mailMessage);
                    OnEmailSent();
                }
                catch (Exception e)
                {
                    //MessageBox.Show(e.Message);
                    Debug.WriteLine(e);
                    OnEmailSent();
                }
            }
            mailMessage.Dispose();
        }

        private void smtpClient_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            OnEmailSent();
        }
    }
}
