﻿namespace Web_Spider_WPF.ViewModels
{
    public class MenuItemViewModel
    {
        public string HotKey { get; set; }

        public string IconPath { get; set; }

        public string Text { get; set; }
    }
}
