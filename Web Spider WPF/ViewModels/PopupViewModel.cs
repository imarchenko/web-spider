﻿using Web_Spider_WPF.Commands;

namespace Web_Spider_WPF.ViewModels
{
    public class PopupViewModel
    {
        public CommandBase ClosePopup { get; set; }

        public PopupViewModel()
        {
            ClosePopup = new CommandBase();
        }
    }
}
