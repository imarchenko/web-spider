﻿namespace Web_Spider_WPF.ViewModels
{
    public class ScanSiteNameColumnViewModel
    {
        public string ScanIcon { get; set; }

        public string ScanName { get; set; }

        public string ScanDescription { get; set; }

        public ScanSiteNameColumnViewModel()
        {
            
        }

        public ScanSiteNameColumnViewModel(string scanIcon, string scanName, string scanDescription)
        {
            ScanIcon = scanIcon;
            ScanName = scanName;
            ScanDescription = scanDescription;
        }
    }
}
