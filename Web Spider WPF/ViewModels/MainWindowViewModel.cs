﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using WebSpiderLib.Models;
using Web_Spider_WPF.Commands;

namespace Web_Spider_WPF.ViewModels
{
    public class MainWindowViewModel
    {
        private const string MenuItemIconsFolder = "/Images/";

        /// <summary>
        /// Комманда закрытия приложения
        /// </summary>
        public CommandBase CloseApplicationCommand { get; set; }

        /// <summary>
        /// Комманда начала заданий по проверке сайтов
        /// </summary>
        public CommandBase StartTasksCommand { get; set; }

        /// <summary>
        /// Комманда остановки заданий по проверке сайтов
        /// </summary>
        public CommandBase StopTasksCommand { get; set; }

        public MenuItemViewModel AboutUsMenuItem { get; set; }

        public MenuItemViewModel HotKeysMenuItem { get; set; }

        public MenuItemViewModel AboutAppMenuItem { get; set; }

        public MenuItemViewModel ExitMenuItem { get; set; }

        public MenuItemViewModel StartTasksMenuItem { get; set; }

        public MenuItemViewModel StopTasksMenuItem { get; set; }

        public MainWindowViewModel()
        {
            CloseApplicationCommand = new CommandBase();
            StartTasksCommand = new CommandBase();
            StopTasksCommand = new CommandBase();
            StartTasksMenuItem = new MenuItemViewModel
            {
                HotKey = "Enter",
                Text = "Начать проверку",
                IconPath = MenuItemIconsFolder + "play_icon.png"
            };
            StopTasksMenuItem = new MenuItemViewModel
            {
                HotKey = "Esc",
                Text = "Остановить проверку",
                IconPath = MenuItemIconsFolder + "stop_icon.png"
            };
            ExitMenuItem = new MenuItemViewModel
            {
                HotKey = "",
                Text = "Выход",
                IconPath = MenuItemIconsFolder + "exit_icon.png"
            };
            AboutUsMenuItem = new MenuItemViewModel
            {
                HotKey = "",
                Text = "О нас",
                IconPath = MenuItemIconsFolder + "info_icon.png"
            };
            AboutAppMenuItem = new MenuItemViewModel
            {
                HotKey = "",
                Text = "О программе",
                IconPath = MenuItemIconsFolder + "info_icon.png"
            };
            HotKeysMenuItem = new MenuItemViewModel
            {
                HotKey = "",
                Text = "Горячие клавиши",
                IconPath = MenuItemIconsFolder + "info_icon.png"
            };
        }

        public DataTemplate CreateScanSiteNameColumnTemplate(DataTemplate dataTemplate)
        {
            //var dataTemplate = new DataTemplate();
            //dataTemplate.
            var grid = dataTemplate.LoadContent() as UIElement;
            if (grid != null)
            {
                var textBlock = FindVisualChild<TextBlock>(grid);
                //var textBlock = grid.FindName("ScanName") as TextBlock;
                var scanNameBinding = new Binding(String.Format("Value[{0}].ScanName", DataGridCellType.Scan_SiteName));
                if (textBlock != null)
                {
                    //textBlock.SetBinding(TextBlock.TextProperty, scanNameBinding);
                    textBlock.Text = "Hi, I am your text binding, woohoo";
                    grid.UpdateLayout();
                    //textBlock.ApplyTemplate();
                }
            }
            return dataTemplate;
        }

        private childItem FindVisualChild<childItem>(DependencyObject obj)
    where childItem : DependencyObject
        {

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {

                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }

            return null;
        }
    }
}
