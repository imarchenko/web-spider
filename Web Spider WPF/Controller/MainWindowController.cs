﻿using System;
using System.Linq;
using WebSpiderLib;
using WebSpiderLib.Models;

namespace Web_Spider_WPF.Controller
{
    public class MainWindowController
    {
        public string[] GetHosts(string str)
        {
            var hosts = str.Trim().Split(new []{"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
            return hosts;
        }

        public SeoParameter[] GetParameters() 
        {
            var parameters = Enum.GetValues(typeof(SeoParameter)) as SeoParameter[];
            return (parameters ?? new SeoParameter[0]).ToArray();
        }

        public ApiResult StartTask(UserTask task)
        {
            var ssEvent = task.Start();
            return task.ParseJson(ssEvent);
        }


    }
}
