﻿using System.Windows;
using System.Windows.Input;
using Web_Spider_WPF.Commands;
using Web_Spider_WPF.ViewModels;

namespace Web_Spider_WPF
{
    /// <summary>
    /// Interaction logic for SignInPopup.xaml
    /// </summary>
    public partial class SignInPopup : Window
    {
        private PopupViewModel _model;

        public SignInPopup()
        {
            InitializeComponent();
            _model = new PopupViewModel();
            _model.ClosePopup = new CommandBase(SignIn);
            DataContext = _model;
            UserName.Focus();
        }

        private void SignInButton_Click(object sender, RoutedEventArgs e)
        {
            SignIn();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void PopupArea_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void SignIn()
        {
            DialogResult = true;
            Close();
        }
    }
}
