﻿namespace Web_Spider_WPF.Commands
{
    public enum CustomCommand
    {
        CloseApplication,
        StartTasks,
        StopTasks,
        ClearResultTables,
        ClearTextBoxWithWebSites
    }
}
