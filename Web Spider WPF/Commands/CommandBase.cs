﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Web_Spider_WPF.Commands
{
    public class CommandBase
    {
        /// <summary>
        /// Действие при вызове комманды
        /// </summary>
        public Action CommandAction;

        private bool _canExecute = true;

        /// <summary>
        /// Задает значение состояния возможности выполнения комманды
        /// </summary>
        public bool CanExecute
        {
            set
            {
                if (Equals(_canExecute, value))
                {
                    return;
                }
                _canExecute = value;
            }
        }

        /// <summary>
        /// Возвращает состояние переключателя возможности выполнения комманды
        /// </summary>
        public ICommand ToggleExecuteCommand { get; set; }

        /// <summary>
        /// Комманда на выполнение
        /// </summary>
        public ICommand Command { get; set; }

        public CommandBase()
        {
            Command = new RelayCommand(Action, param => _canExecute);
            ToggleExecuteCommand = new RelayCommand(ChangeCanExecute);
            CommandAction = () => MessageBox.Show("Not implemented command action");
        }

        public CommandBase(Action action, bool canExecute = true) : this()
        {
            CommandAction = action;
            CanExecute = canExecute;
        }

        public void Action(object obj)
        {
            CommandAction();
        }

        public void ChangeCanExecute(object obj)
        {
            _canExecute = !_canExecute;
        }
    }
}
