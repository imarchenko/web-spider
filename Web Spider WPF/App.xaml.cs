﻿using System.Diagnostics;
using System.IO;
using System.Windows;

namespace Web_Spider_WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        const string BugReporterAppName = "BugReporter.exe";

        public App()
        {
            Dispatcher.UnhandledException += Dispatcher_UnhandledException;
        }

        void Dispatcher_UnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            var errorMessages = new[] {string.Format("An unhandled exception occurred: {0}", e.Exception)};
            const string logPath = "Logs.txt";
            if (!File.Exists(logPath))
            {
                File.Create(logPath);
            }
            File.WriteAllLines(logPath, errorMessages);
            if (File.Exists(BugReporterAppName))
            {
                Process.Start(BugReporterAppName); 
            }
        }
    }
}
