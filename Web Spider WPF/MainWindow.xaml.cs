﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using WebSpiderLogger.Models;
using Web_Spider_WPF.Commands;
using Web_Spider_WPF.Controller;
using WebSpiderLib;
using WebSpiderLib.Models;
using WebSpiderLogger;
using WebSpiderLib.Extensions;
using Web_Spider_WPF.ViewModels;

namespace Web_Spider_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<BackgroundWorker> _taskWorkers;
        private MainWindowController _controller;
        private string[] _hosts;
        private ObservableDictionary<SeoParameter, ObservableDictionary<string, object>> _dataGridSource;
        private Logger _logger;
        private ObservableCollection<string> _logs;
        private MainWindowViewModel _model;
        private IList<SeoParameter> _seoParameters; 


        private double TaskProgress { get; set; }
        private double ProgressStep { get; set; }

        public MainWindow()
        {
            _controller = new MainWindowController();
            _taskWorkers = new List<BackgroundWorker>();
            _logger = new Logger();
            _logs = new ObservableCollection<string>();
            _hosts = new string[0];
            
            //var currentDomain = AppDomain.CurrentDomain;
            //currentDomain.UnhandledException += currentDomain_UnhandledException;

            InitializeComponent();

            _model = new MainWindowViewModel
            {
                CloseApplicationCommand = new CommandBase(CloseApplication),
                StartTasksCommand = new CommandBase(StartTasks),
                StopTasksCommand = new CommandBase(StopTasks, false)
            };
            DataContext = _model;
            InitDataGridSeoSource();

            LB_Logs.ItemsSource = _logs;
        }

        //void currentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        //{
        //    _logger.AddFatalError(FatalErrorType.UknownFatalError, e.ExceptionObject as Exception);
        //    _taskWorkers.ForEach(worker => worker.CancelAsync());
        //    MessageBox.Show("Произошла неизвестная ошибка!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        //}

        private void InitDataGridSeoSource()
        {
            DG_Seo.AutoGenerateColumns = false;
            _dataGridSource = new ObservableDictionary<SeoParameter, ObservableDictionary<string, object>>();
            DG_Seo.ItemsSource = _dataGridSource;

            var idBinding = new Binding(String.Format("Value[{0}]", DataGridCellType.Id));
            var idColumn = new DataGridTextColumn
            {
                Binding = idBinding,
                Header = DataGridCellType.Id.GetLocalizedDisplayString(),
                CellStyle = TryFindResource("DataGridIdColumnStyle") as Style
            };
            DG_Seo.Columns.Add(idColumn);

            //var parameterBinding = new Binding(String.Format("Value[{0}]", DataGridCellType.Scan_SiteName));
            var columnTemplate = FindResource("ScanColumnTemplate") as DataTemplate;
            //if (columnTemplate != null)
            //{
            //    var grid = columnTemplate.LoadContent() as Grid;
            //    if (grid != null)
            //    {
            //        grid.DataContext = new ScanSiteNameColumnViewModel();
            //    }
            //    //columnTemplate.RegisterName("current", DataGridCellType.Scan_SiteName.ToString()); 
            //}
            columnTemplate = _model.CreateScanSiteNameColumnTemplate(columnTemplate);
            DG_Seo.Columns.Add(new DataGridTemplateColumn
            {
                CellTemplate = columnTemplate,
                Header = DataGridCellType.Scan_SiteName.GetLocalizedDisplayString()
            });
            
            _seoParameters = _controller.GetParameters();
            for (var rowIndex = 0; rowIndex < _seoParameters.Count; rowIndex++)
            {
                var parameter = _seoParameters[rowIndex];
                var row = InitDataGridRow();
                row[DataGridCellType.Id.ToString()] = (rowIndex + 1);

                //var data = new Tuple<string, string>("/Images/SeoIcons/" + parameter.GetIconName(), parameter.GetLocalizedDisplayString());
                var scanViewModel = new ScanSiteNameColumnViewModel
                {
                    ScanIcon = "/Images/SeoIcons/" + parameter.GetIconName(),
                    ScanName = parameter.GetLocalizedDisplayString(),
                    ScanDescription = parameter.GetDescription()
                };
                //var dataTemplate = row[DataGridCellType.Scan_SiteName.ToString()] as DataTemplate;
                row[DataGridCellType.Scan_SiteName.ToString()] = scanViewModel;
                _dataGridSource.Add(parameter, row);
                
            }
        }

        public void AddSitesToDataGrid()
        {
            foreach (var host in _hosts)
            {
                var binding = new Binding(String.Format("Value[{0}]", host));
                DG_Seo.Columns.Add(new DataGridTextColumn
                {
                    Binding = binding,
                    Header = host
                });
            }
        }

        private ObservableDictionary<string, object> InitDataGridRow()
        {
            var defaultValue = AppSettings.Default.TaskResultDefaultValue;
            var cells = _hosts.ToDictionary(key => key, value => defaultValue as object);
            cells.Add(DataGridCellType.Id.ToString(), defaultValue);
            cells.Add(DataGridCellType.Scan_SiteName.ToString(), defaultValue);
            return new ObservableDictionary<string, object>(cells);
        }

        private BackgroundWorker CreateWorker()
        {
            var backgroundWorker = new BackgroundWorker {WorkerSupportsCancellation = true};
            backgroundWorker.DoWork += _tasksWorker_DoWork;
            backgroundWorker.RunWorkerCompleted += _tasksWorker_RunWorkerCompleted;
            return backgroundWorker;
        }

        void _tasksWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            if (!e.Cancelled && worker != null && !worker.CancellationPending)
            {
                var apiResult = e.Result as ApiResult;
                if (apiResult == null)
                {
                    return;
                }
                if (_taskWorkers.Contains(worker))
                {
                    try
                    {
                        var apiTask = apiResult.TaskResult;
                        var errors = new List<ILog>();
                        errors.AddRange(_logger.GetErrors());
                        errors.AddRange(_logger.GetFatalErrors());
                        if (errors.Any())
                        {
                            foreach (var error in errors)
                            {
                                _logs.Add(error.Message);
                                LB_Logs.ScrollIntoView(_logs.Last());
                            }
                        }
                        if (_dataGridSource.ContainsKey(apiTask.Id))
                        {
                            var row = _dataGridSource[apiTask.Id];
                            if (row != null)
                            {
                                row[apiResult.Host] = apiResult.TaskResult.Result.ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.AddError(String.Format("Неудалось завершить задание по параметру {0} сайта {1}: {2}",
                            apiResult.TaskResult.Id.GetLocalizedDisplayString(), apiResult.Host, ex.Message));
                    }
                    AddProgress();
                    _taskWorkers.Remove(worker);
                } 
            }
            if (!_taskWorkers.Any())
	        {
                HideProgress();
	        }
            if (worker != null)
            {
                worker.Dispose(); 
            }
        }

        void _tasksWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            var task = e.Argument as UserTask;
            try
            {
                if (worker != null && !worker.CancellationPending)
                {
                    e.Result = _controller.StartTask(task);
                }
            }
            catch (Exception ex)
            {
                _logger.AddError("Задание завершилось с ошибкой: " + ex.Message);
                if (worker != null)
                {
                    _taskWorkers.Remove(worker);
                    worker.Dispose();
                }
            }
            finally
            {
                if (task != null)
                {
                    task.Dispose();
                }
            }
        }

        private void AddProgress()
        {
            TaskProgress = SetTasksProgress(TaskProgress + ProgressStep);
        }

        private double SetTasksProgress(double progress)
        {
            PB_TasksProgress.Value = progress;
            L_TasksProgress.Content = (int)progress + "%";
            return progress;
        }
        
        private void StartTasks()
        {
            try
            {
                _hosts = _controller.GetHosts(new TextRange(RTB_Hosts.Document.ContentStart, RTB_Hosts.Document.ContentEnd).Text);
                if (_hosts == null || !_hosts.Any())
                {
                    return;
                }
                ClearUI();
                ShowProgress();
                AddSitesToDataGrid();
                ProgressStep = 100.00 / (_hosts.Length * _seoParameters.Count);
                for (var rowIndex = 0; rowIndex < _seoParameters.Count; rowIndex++)
                {
                    var parameter = _seoParameters[rowIndex];
                    foreach (var host in _hosts)
                    {
                        _dataGridSource[parameter][host] = AppSettings.Default.TaskResultDefaultValue;
                        var task = new UserTask(rowIndex, AppSettings.Default.SSEStreamUrl, host, parameter, _logger);
                        var backgroundWorker = CreateWorker();
                        _taskWorkers.Add(backgroundWorker);
                        backgroundWorker.RunWorkerAsync(task);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.AddFatalError(FatalErrorType.UknownFatalError, ex);
                StopTasks();
            }
        }

        private void StopTasks()
        {
            foreach (var task in _taskWorkers)
            {
                task.CancelAsync();
                task.Dispose();
            }
            _taskWorkers.Clear();
            HideProgress();
        }

        private void ClearUI()
        {
            StopTasks();
            _logs.Clear();
            foreach (var row in _dataGridSource.Values)
            {
                var hostColumnsData = row.Where(_column => _column.Key != DataGridCellType.Id.ToString()
                                        && _column.Key != DataGridCellType.Scan_SiteName.ToString()).ToArray();
                foreach (var hostColumn in hostColumnsData)
                {
                    row.Remove(hostColumn);
                }
            }
            var hostColumns = DG_Seo.Columns.Where(_column => !_column.IsFrozen).ToArray();
            foreach (var hostColumn in hostColumns)
            {
                DG_Seo.Columns.Remove(hostColumn);
            }
            TaskProgress = SetTasksProgress(0);
            L_TasksProgress.Content = string.Empty;
        }

        private void ShowProgress()
        {
            _model.StopTasksCommand.CanExecute = true;
            _model.StartTasksCommand.CanExecute = false;
            L_TasksProgress.Visibility = Visibility.Visible;
            PB_TasksProgress.Visibility = Visibility.Visible;
            B_StopTasks.Visibility = Visibility.Visible;
        }

        private void HideProgress()
        {
            _model.StopTasksCommand.CanExecute = false;
            _model.StartTasksCommand.CanExecute = true;
            L_TasksProgress.Visibility = Visibility.Collapsed;
            PB_TasksProgress.Visibility = Visibility.Collapsed;
            B_StopTasks.Visibility = Visibility.Collapsed;
            RTB_Hosts.Focus();
        }

        private void RTB_Hosts_OnKeyDown(object sender, KeyEventArgs e)
        {
            var richTextBox = sender as RichTextBox;
            if (richTextBox == null)
            {
                return;
            }
            if (e.Key == Key.Escape)
            {
                if (_taskWorkers.Any())
                {
                    StopTasks();
                }
                else
                {
                    richTextBox.Document.Blocks.Clear(); 
                }
            }
            else if (e.Key == Key.Enter)
            {
                if (e.KeyboardDevice.Modifiers == ModifierKeys.Control || e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
                {
                    richTextBox.CaretPosition = richTextBox.CaretPosition.InsertLineBreak();
                }
                else
                {
                    StartTasks();
                }
            }
        }

        private void DG_Seo_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var dataGrid = sender as DataGrid;
            if (dataGrid != null && dataGrid.SelectedCells.Any())
            {
                dataGrid.UnselectAll();
            }
        }

        private void CloseApplication()
        {
            StopTasks();
            Close();
        }

        private void RegisterLink_OnClick(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://cqr.com.ua");
        }

        private void SignInButton_OnClick(object sender, RoutedEventArgs e)
        {
            var popup = new SignInPopup();
            var result = popup.ShowDialog();
            if (result.GetValueOrDefault())
            {
                UserName.Content = popup.UserName.Text;
                AuthorizationPanel.Visibility = Visibility.Collapsed;
                ProfilePanel.Visibility = Visibility.Visible;
            }
        }

        private void SignOutButton_OnClick(object sender, RoutedEventArgs e)
        {
            AuthorizationPanel.Visibility = Visibility.Visible;
            ProfilePanel.Visibility = Visibility.Collapsed;
        }
    }
}
