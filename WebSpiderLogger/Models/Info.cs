﻿using System;

namespace WebSpiderLogger.Models
{
    public class Info : LogBase
    {
        public Info() : base(LogType.Info) { }

        public Info(string message) : base(LogType.Info, message) { }

        public Info(Exception exception) : base(LogType.Info, exception) { }
    }
}
