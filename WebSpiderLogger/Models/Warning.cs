﻿using System;

namespace WebSpiderLogger.Models
{
    public class Warning : LogBase
    {
        public Warning() : base(LogType.Warning) { }

        public Warning(string message) : base(LogType.Warning, message) { }

        public Warning(Exception exception) : base(LogType.Warning, exception) { }
    }
}
