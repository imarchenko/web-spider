﻿using System;

namespace WebSpiderLogger.Models
{
    public class FatalError : LogBase
    {
        public FatalErrorType FatalErrorType { get; protected set; }
        
        public FatalError() : base(LogType.Fatal) { }
        
        public FatalError(FatalErrorType fatalErrorType, string message) : base(LogType.Fatal, message)
        {
            FatalErrorType = fatalErrorType;
        }

        public FatalError(FatalErrorType fatalErrorType, Exception exception) : base(LogType.Fatal, exception) 
        {
            FatalErrorType = fatalErrorType;
        }
    }
}
