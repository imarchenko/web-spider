﻿using System;

namespace WebSpiderLogger.Models
{
    public class Error : LogBase
    {
        public Error() : base(LogType.Error) { }

        public Error(string message) : base(LogType.Error, message) { }

        public Error(Exception exception) : base(LogType.Error, exception) { }
    }
}
