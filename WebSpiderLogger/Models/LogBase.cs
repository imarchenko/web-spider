﻿using System;

namespace WebSpiderLogger.Models
{
    public abstract class LogBase : ILog
    {
        public LogType Type { get; set; }

        public string Message { get; set; }

        public Exception Exception { get; set; }

        public bool IsNew { get; set; }

        protected LogBase(LogType type)
        {
            Message = type.ToString();
            Type = type;
            IsNew = true;
        }

        protected LogBase(LogType type, string message) : this(type)
        {
            Message = message ?? string.Empty;
        }

        protected LogBase(LogType type, Exception exception) : this(type)
        {
            Exception = exception;
            Message = type + ": " + exception.Message;
        }
    }
}
