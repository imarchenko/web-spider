﻿using System.ComponentModel.DataAnnotations;

namespace WebSpiderLogger.Models
{
    /// <summary>
    /// Перечисление ситуаций при которых все задачи необходимо остановить
    /// </summary>
    public enum FatalErrorType
    {
        /// <summary>
        /// Не удается подключиться к серверу
        /// </summary>
        [Display(Name = "Не известная ошибка")]
        UknownFatalError = 0,
        /// <summary>
        /// Не удается подключиться к интернету
        /// </summary>
        [Display(Name = "Не удается подключиться к интернету")]
        InternetConnectionFailed = 1,
        /// <summary>
        /// Не удается подключиться к серверу
        /// </summary>
        [Display(Name = "Не удается подключиться к серверу")]
        ApiConnectionFailed = 2
    }
}
