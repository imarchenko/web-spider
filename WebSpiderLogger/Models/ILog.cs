﻿using System;

namespace WebSpiderLogger.Models
{
    public interface ILog
    {
        LogType Type { get; set; }

        string Message { get; set; }

        Exception Exception { get; set; }

        bool IsNew { get; set; }
    }
}
