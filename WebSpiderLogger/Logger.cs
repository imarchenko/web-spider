﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebSpiderLogger.Models;

namespace WebSpiderLogger
{
    public class Logger
    {
        private List<ILog> _Logs { get; set; }

        public Logger() 
        {
            _Logs = new List<ILog>();
        }

        public void AddFatalError(FatalErrorType fatalErrorType, string message)
        {
            _Logs.Add(new FatalError(fatalErrorType, message));
        }

        public void AddFatalError(FatalErrorType fatalErrorType, Exception exception)
        {
            _Logs.Add(new FatalError(fatalErrorType, exception));
        }

        public void AddError(string message)
        {
            _Logs.Add(new Error(message));
        }

        public void AddError(Exception exception)
        {
            _Logs.Add(new Error(exception));
        }

        public void AddWarning(string message)
        {
            _Logs.Add(new Warning(message));
        }

        public void AddInfo(string message)
        {
            _Logs.Add(new Info(message));
        }

        public IEnumerable<ILog> GetNewLogs(LogType type)
        {
            var logs = _Logs.Where(_item => _item.IsNew && _item.Type == type);
            var newLogs = logs as ILog[] ?? logs.ToArray();
            MarkAsRead(logs);
            return newLogs;
        }

        public IEnumerable<ILog> GetFatalErrors()
        {
            var logs = GetNewLogs(LogType.Fatal).Where(_item => _item.Type == LogType.Fatal);
            return logs;
        }

        public IEnumerable<ILog> GetErrors()
        {
            var logs = GetNewLogs(LogType.Error).Where(_item => _item.Type == LogType.Error);
            return logs;
        }

        public IEnumerable<ILog> GetWarnings()
        {
            var logs = GetNewLogs(LogType.Warning).Where(_item => _item.Type == LogType.Warning);
            return logs;
        }

        public IEnumerable<ILog> GetInfos()
        {
            var logs = GetNewLogs(LogType.Info).Where(_item => _item.Type == LogType.Info);
            return logs;
        }

        private IEnumerable<ILog> MarkAsRead(IEnumerable<ILog> logs)
        {
            foreach (var log in logs)
            {
                log.IsNew = false;
            }
            return logs;
        }
    }
}
